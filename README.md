# README #

### What is this repository for? ###

The project is example of usage new CMake "CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS" option which helps to build shared libraries on windows with usage of MSVC compiler without explicit necessity to add export macros or creating .def file by hand.

More info about this is provided by link:
[Export all symbols when creating a DLL](http://stackoverflow.com/questions/225432/export-all-symbols-when-creating-a-dll)

### Requirements ###
* [CMake](http://www.cmake.org/files/dev/?C=M;O=D) 3.3.20150721 or newer.
* [Visual Studio 2013](https://www.visualstudio.com/en-us/downloads/download-visual-studio-vs.aspx).

### Installation ###

* To set up the project you have to use "CMake (cmake-gui)"
* Set up project with "CMake (cmake-gui)": 1)  Where is the source code: set path to the project folder Where to build the binaries: set path to the project folder/build 2) Click Configure, then Generate.